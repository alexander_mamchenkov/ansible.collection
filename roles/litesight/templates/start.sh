#!/bin/bash

env     BITCOIND_HOST={{ mfcoind_rpc_host }} \
        BITCOIND_PORT={{ mfcoind_rpc_port }} \
        BITCOIND_P2P_PORT={{ mfcoind_p2p_port }} \
        BITCOIND_USER={{ mfcoind_rpc_user }} \
        BITCOIND_PASS="{{ mfcoind_rpc_pass }}" \
        BITCOIN_DATADIR={{ mfcoind_data_path }} \
        INSIGHT_NETWORK=livenet \
        ~/.nvm/{{ mfcoind_data_path }}/bin/npm start
