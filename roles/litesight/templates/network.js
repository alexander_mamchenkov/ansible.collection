exports.livenet = {
  name: 'livenet',
  magic: hex('d4caafeb'),
  addressVersion: 51,
  privKeyVersion: 179,
  P2SHVersion: 5,
  hkeyPublicVersion: 0x0488b21e,
  hkeyPrivateVersion: 0x0488ade4,
  genesisBlock: {
    hash: hex('06da6d3817b3138d6e887a7b1ca48bae057965a5638fced52f7010ce857ba15c'),
    merkle_root: hex('b5c4d139ee5f25ffbbe664eb17d567150cc7cb34fb3f7f561be6d010a2b5fd3b'),
    height: 0,
    nonce: 1827771,
    version: 1,
    prev_hash: buffertools.fill(new Buffer(32), 0),
    timestamp: 1501719878,
    bits: 0x1e0ffff0
  },
  dnsSeeds: [
    'd1.cree.net'
  ],
  defaultClientPort: "{{ mfcoind_rpc_port }}",
  lastPoWBlock: 999999999
};

